#pragma once
#include "Vertex.h"
#include <vector>
#include "GameObject.h"

#define PLATFORM 0
#define BALL 1
#define COLLIDERS 2
#define FONDO 3
#define NUMBASICOBJECTS 4

//This class stores and manipulates all the objects loaded from the text file
class Geometry
{
	Vertex * _verticesData[NUMBASICOBJECTS];
	int _numVertices[NUMBASICOBJECTS];
	std::vector <GameObject> _listOfObjects;
	

public:
	Geometry();
	~Geometry();
	void loadGameElements(char fileName[100]);
	void deleteGameElement(int pos);
	Vertex * getData(int objectID);
	int getNumVertices(int objectID);
	int getNumGameElements();
	GameObject & getGameElement(int objectID);
};

