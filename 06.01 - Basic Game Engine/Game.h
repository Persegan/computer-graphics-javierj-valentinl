#pragma once


//Third-party libraries
#include <GL/glew.h>			//The OpenGL Extension Wrangler
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>			//OpenGL Mathematics 
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include "Window.h"
#include "GLSLProgram.h"
#include "FPSLimiter.h"
#include "OpenGLBuffers.h"
#include "Vertex.h"
#include "Geometry.h"
#include "InputManager.h"
#include "Camera.h"
#include "TextureManager.h"
#include "MaterialManager.h"

#define ORIGINAL_COLOR 0
#define TEXTURE_COLOR 1
#define COMBINED_COLOR 2
#define DRAW_MODE 3
#define LIGHT_MODE 2

#define FIST_CAMERA 0
#define SECOND_CAMERA 1
#define NUM_CAMERAS 2

//Game has four possible states: INIT (Preparing environment), PLAY (Playing), EXIT (Exit from the game) or MENU (Game menu)
enum class GameState{INIT, PLAY, EXIT, MENU};

//This class manages the game execution
class Game {
	public:						
		Game(std::string windowTitle, int screenWidth, int screenHeight, bool enableLimiterFPS, int maxFPS, bool printFPS);	//Constructor
		~Game();					//Destructor
		void run();					//Game execution

	private:
			//Attributes	
		std::string _windowTitle;		//Window Title
		int _screenWidth;				//Screen width in pixels				
		int _screenHeight;				//Screen height in pixels				
		GameState _gameState;			//It describes the game state				
		Window _window;					//Manage the OpenGL context
		GLSLProgram _colorProgram;		//Manage the shader programs
		FPSLimiter _fpsLimiter;			//Manage the synchronization between frame rate and refresh rate
		OpenGLBuffers _openGLBuffers;	//Manage the openGL buffers
		Geometry _gameElements;			//Manage the game elements
		InputManager _inputManager;		//Manage the input devices

		TextureManager _textureManager;

		GLuint _drawModeUniform;
		GLuint _lightModeUniform;
		GLuint _newColorUniform;
		GLint _textureDataLocation;
		GLint _textureScaleFactorLocation;
		GLuint _isALightSource;
		GLuint _lightPosition;
		GLuint _viewerPosition;
	//	GLuint _material;
	//	GLuint _lightColor;

		MaterialManager _materialManager;

		GLuint _materialAmbientUniform;
		GLuint _materialDiffuseUniform;
		GLuint _materialSpecularUniform;
		GLuint _materialShininessUniform;

		GLuint _lightColorAmbientUniform;
		GLuint _lightColorDiffuseUniform;
		GLuint _lightColorSpecularUniform;


		bool gameStart =true;
		bool left = false;
		bool right = false;
		bool up = false;
		float _movex = 0;
		float _movey = 0.5;
		float _score = 0;
		int _cameraID;
		Camera _cameras[3];

		int _drawMode;

		int _lightMode;
	
		GLuint _modelMatrixUniform;
		GLuint _viewMatrixUniform;
		GLuint _projectionMatrixUniform;

		GLuint _modelNormalMatrixUniform;

			//Internal methods
		void initSystems();
		void initShaders();		
		void gameLoop();
		void processInput();
		void doPhysics();
		void executePlayerCommands();
		void renderGame();			

		void loadGameTextures();
};

