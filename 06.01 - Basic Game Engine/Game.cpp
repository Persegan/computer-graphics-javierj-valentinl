#include "Game.h"


/*
Pasos a realizar:
1. Completar funci�n loadgameElements: Tiene que leer del archivo de texto y guardar lo que lee en el vector listofobjects. Usar el push para incrementar el tama�o del vector.
El texto funciona as�: n�mero del objeto (definido por el define), matriz de traslaci�n, �ngulo de rotaci�n, matriz de rotaci�n y matriz de escalado

2. En el constructor de Geometry rellenar num_vertices y vertices_data para cada objeto b�sico y hacer el setposition y setcolor de todos ellos

3. Poner que roten con la teclas. Para eso modificar valores de la clase GameObject.

4. Poner Matrix Model en el render game del game.cpp

*/



//Hay que inicializar la view matrix de la c�mara. 

/*
* Constructor
* Note: It uses an initialization list to set the parameters
* @param windowTitle is the window title
* @param screenWidth is the window width
* @param screenHeight is the window height
*/
Game::Game(std::string windowTitle, int screenWidth, int screenHeight, bool enableLimiterFPS, int maxFPS, bool printFPS) :
	_windowTitle(windowTitle), 
	_screenWidth(screenWidth), 
	_screenHeight(screenHeight),
	_gameState(GameState::INIT), 
	_fpsLimiter(enableLimiterFPS, maxFPS, printFPS),
	_drawMode(TEXTURE_COLOR) {


}

/**
* Destructor
*/
Game::~Game()
{
}

/*
* Game execution
*/
void Game::run() {
		//System initializations
	initSystems();
		//Start the game if all the elements are ready
	gameLoop();
}

/*
* Initializes all the game engine components
*/
void Game::initSystems() {
		//Create an Opengl window using SDL
	_window.create(_windowTitle, _screenWidth, _screenHeight, 0);		
		//Compile and Link shader
	initShaders();
		//Set up the openGL buffers
	_openGLBuffers.initializeBuffers(_colorProgram);
		//Load the current scenario
	_gameElements.loadGameElements("./resources/scene2D.txt");
	_cameras[0] = Camera(_screenWidth / _screenHeight, 3.0f, 300.0f, 0.1f, 30.0f, 30.0f, false, glm::vec3(0.0f, 0.0f, 10.0f), glm::vec3(0.0f, 0.01f, 0.0f));
	_cameras[1] = Camera(_screenWidth / _screenHeight, 3.0f, 300.0f, 0.1f, 30.0f, 30.0f, false, glm::vec3(0.5f, 0.5f, 0.0f), glm::vec3(0.0f, 0.0f, 0.5f));
	_cameras[2] = Camera(_screenWidth / _screenHeight, 3.0f, 300.0f, 0.1f, 30.0f, 30.0f, true, glm::vec3(0.5f, 0.5f, 0.0f), glm::vec3(0.0f, 0.0f, 0.5f)); 

	_cameraID = 0;

	loadGameTextures();
}

/*
* Initialize the shaders:
* Compiles, sets the variables between C++ and the Shader program and links the shader program
*/
void Game::initShaders() {
		//Compile the shaders
	_colorProgram.addShader(GL_VERTEX_SHADER, "./resources/shaders/vertex-shader.txt");
	_colorProgram.addShader(GL_FRAGMENT_SHADER, "./resources/shaders/fragment-shader.txt");
	_colorProgram.compileShaders();
		//Attributes must be added before linking the code
	_colorProgram.addAttribute("vertexPositionGame");
//	_colorProgram.addAttribute("vertexColor");
	_colorProgram.addAttribute("vertexUV");
	_colorProgram.addAttribute("vertexNormal");
		//Link the compiled shaders
	_colorProgram.linkShaders();
		//Bind the uniform variables. You must enable shaders before gettting the unifomre variable location
	_colorProgram.use();
	_modelMatrixUniform = _colorProgram.getUniformLocation("modelMatrix");
	 _projectionMatrixUniform = _colorProgram.getUniformLocation("projectionMatrix");
	 _viewMatrixUniform = _colorProgram.getUniformLocation("viewMatrix");

	 _modelNormalMatrixUniform = _colorProgram.getUniformLocation("modelNormalMatrix");

	 _drawModeUniform = _colorProgram.getUniformLocation("drawMode");
	 _newColorUniform = _colorProgram.getUniformLocation("objectColor");
	 _textureDataLocation = _colorProgram.getUniformLocation("textureData");
	 _textureScaleFactorLocation = _colorProgram.getUniformLocation("textureScaleFactor");
	 _lightModeUniform = _colorProgram.getUniformLocation("lightingEnabled");
	 _isALightSource = _colorProgram.getUniformLocation("isALightSource");
	 _lightPosition = _colorProgram.getUniformLocation("lightPosition");

	 _materialAmbientUniform = _colorProgram.getUniformLocation("material.ambient");
	 _materialDiffuseUniform = _colorProgram.getUniformLocation("material.diffuse");
	 _materialSpecularUniform = _colorProgram.getUniformLocation("material.specular");
	 _materialShininessUniform = _colorProgram.getUniformLocation("material.shininess");
	 _viewerPosition = _colorProgram.getUniformLocation("viewerPosition");


	 _lightColorAmbientUniform = _colorProgram.getUniformLocation("lightColor.ambient");
	 _lightColorDiffuseUniform = _colorProgram.getUniformLocation("lightColor.diffuse");
	 _lightColorSpecularUniform = _colorProgram.getUniformLocation("lightColor.specular");


	// _material = _colorProgram.getUniformLocation("material");
	 //_lightColor = _colorProgram.getUniformLocation("lightColor");

	_colorProgram.unuse();

}

void Game::loadGameTextures() {

	/*Suggestion:
	- You can define the texture fileNames in the "scene3D.txt" file.
	- Next, you can loop through the GameObjects stored in the Geometry class, and assign the textureID in each GameObject
	*/

	GameObject currentGameObject;
	//Load the game textures			
	for (int i = 0; i < _gameElements.getNumGameElements(); i++) {
		currentGameObject = _gameElements.getGameElement(i);
		(_gameElements.getGameElement(i))._textureID = _textureManager.getTextureID(currentGameObject._textureFile);
	}


}

/*
* Game execution: Gets input events, processes game logic and draws sprites on the screen
*/
void Game::gameLoop() {	
	_gameState = GameState::PLAY;
	while (_gameState != GameState::EXIT) {		
			//Start synchronization between refresh rate and frame rate
		_fpsLimiter.startSynchronization();
			//Process the input information (keyboard and mouse)
		processInput();
			//Execute the player actions (keyboard and mouse)
		executePlayerCommands();
			//Update the game status
		doPhysics();
			//Draw the objects on the screen
		renderGame();	
			//Force synchronization
		_fpsLimiter.forceSynchronization();
	}
}

/*
* Processes input with SDL
*/
void Game::processInput() {
	_inputManager.update();
	//Review https://wiki.libsdl.org/SDL_Event to see the different kind of events
	//Moreover, table show the property affected for each event type
	SDL_Event evnt;
	//Will keep looping until there are no more events to process
	while (SDL_PollEvent(&evnt)) {
		switch (evnt.type) {
		case SDL_QUIT:
			_gameState = GameState::EXIT;
			break;
		case SDL_MOUSEMOTION:
			_inputManager.setMouseCoords(evnt.motion.x, evnt.motion.y);
			break;
		case SDL_KEYDOWN:
			_inputManager.pressKey(evnt.key.keysym.sym);
			break;
		case SDL_KEYUP:
			_inputManager.releaseKey(evnt.key.keysym.sym);
			break;
		case SDL_MOUSEBUTTONDOWN:
			_inputManager.pressKey(evnt.button.button);
			break;
		case SDL_MOUSEBUTTONUP:
			_inputManager.releaseKey(evnt.button.button);
			break;
		default:
			break;
		}
	}

}


/**
* Executes the actions sent by the user by means of the keyboard and mouse
*/
void Game::executePlayerCommands() {

	if (_inputManager.isKeyPressed(SDLK_t)) {
		_drawMode = (_drawMode + 1) % DRAW_MODE;
	}
	
	if (_inputManager.isKeyPressed(SDLK_l)) {
		_lightMode = (_lightMode + 1) % LIGHT_MODE;
	}


	if (_inputManager.isKeyPressed(SDL_BUTTON_LEFT)){
		glm::ivec2 mouseCoords = _inputManager.getMouseCoords();
		cout << mouseCoords.x << ", " << mouseCoords.y << endl;
	}

	if (_inputManager.isKeyPressed(SDLK_w)) {
		gameStart = false;
	}

	if (_inputManager.isKeyPressed(SDLK_a)) {
		_gameElements.getGameElement(2)._translate.x -= 2.5;
		left = true;
		right = false;

		if (gameStart) {
			_gameElements.getGameElement(1)._translate.x -= 2.5; _movex = -0.5;
		}
			
		
	}
	if (_inputManager.isKeyPressed(SDLK_s)) {
		if (gameStart) {
			_movex = 0;
		}
	}

	if (_inputManager.isKeyPressed(SDLK_d)) {
		_gameElements.getGameElement(2)._translate.x += 2.5; 
		left = false;
		right = true;

		if (gameStart) {
			_gameElements.getGameElement(1)._translate.x += 2.5; _movex = 0.5;
		}
			
	}
	if (_inputManager.isKeyPressed(SDLK_v)) {
		_cameraID= (_cameraID+1)%3;
	}
	

}

/*
* Update the game objects based on the physics
*/
void Game::doPhysics() {
	static int counter = 0;


	if (!gameStart) {
		
		if (_score == 11) {
			gameStart = !gameStart; _cameraID = 2;
			std::cout << "YOU WIN, YEAHH :)";
		}
		_gameElements.getGameElement(1)._translate.y += _movey; _gameElements.getGameElement(1)._translate.x += _movex;

		if (_gameElements.getGameElement(1)._translate.y > 15)
			_movey = -_movey;
		else if (_gameElements.getGameElement(1)._translate.y < -15) {
			gameStart = !gameStart; _cameraID = 2;
			std::cout<<std::endl << "GAME OVER";
		}
			

		if (_gameElements.getGameElement(1)._translate.x < -15 || _gameElements.getGameElement(1)._translate.x > 15) {
			_movex = -_movex;
		}
		
		if (_gameElements.getGameElement(1)._translate.y < -14 && _gameElements.getGameElement(1)._translate.x> _gameElements.getGameElement(2)._translate.x-5 && _gameElements.getGameElement(1)._translate.x < (_gameElements.getGameElement(2)._translate.x)) {
			_movey = -_movey; 
			if (left) _movex = -0.5;
			if (right) _movex = 0.5;

		}

		//CHECK BRICK 0
		if (_gameElements.getGameElement(1)._translate.y > _gameElements.getGameElement(0)._translate.y-2 && _gameElements.getGameElement(1)._translate.y < _gameElements.getGameElement(0)._translate.y-1 
			&& _gameElements.getGameElement(1)._translate.x > _gameElements.getGameElement(0)._translate.x - 2.4 && _gameElements.getGameElement(1)._translate.x < _gameElements.getGameElement(0)._translate.x - 0.1){ 
			_movey = -_movey; 
			_gameElements.getGameElement(0)._translate.y = 100; 
			_score++;
			std::cout << std::endl << "  YOUR SCORE IS: " << _score << std::endl;
		}
		if (_gameElements.getGameElement(1)._translate.y > _gameElements.getGameElement(0)._translate.y - 2 && _gameElements.getGameElement(1)._translate.y < _gameElements.getGameElement(0)._translate.y - 1
			&& _gameElements.getGameElement(1)._translate.x == _gameElements.getGameElement(0)._translate.x - 2.5) {
			_movex = -_movex;
			_gameElements.getGameElement(0)._translate.y = 100;
			_score++;
			std::cout << std::endl << "  YOUR SCORE IS: " << _score << std::endl;
		}
		if (_gameElements.getGameElement(1)._translate.y > _gameElements.getGameElement(0)._translate.y - 2 && _gameElements.getGameElement(1)._translate.y < _gameElements.getGameElement(0)._translate.y - 1
			&& _gameElements.getGameElement(1)._translate.x == _gameElements.getGameElement(0)._translate.x) {
			_movex = -_movex;
			_gameElements.getGameElement(0)._translate.y = 100;
			_score++;
			std::cout << std::endl << "  YOUR SCORE IS: " << _score << std::endl;
		}
		//CHECK REST OF BRICKS
		for (int i = 4; i < 14; i++) {
		
			if (_gameElements.getGameElement(1)._translate.y > _gameElements.getGameElement(i)._translate.y - 2 && _gameElements.getGameElement(1)._translate.y < _gameElements.getGameElement(i)._translate.y - 1
				&& _gameElements.getGameElement(1)._translate.x > _gameElements.getGameElement(i)._translate.x - 2.4 && _gameElements.getGameElement(1)._translate.x < _gameElements.getGameElement(i)._translate.x - 0.1) {
				_movey = -_movey;
				_gameElements.getGameElement(i)._translate.y = 100;
				_score++;
				std::cout << std::endl << "  YOUR SCORE IS: " << _score << std::endl;
			}
			if (_gameElements.getGameElement(1)._translate.y > _gameElements.getGameElement(i)._translate.y - 2 && _gameElements.getGameElement(1)._translate.y < _gameElements.getGameElement(i)._translate.y - 1
				&& _gameElements.getGameElement(1)._translate.x == _gameElements.getGameElement(i)._translate.x - 2.5) {
				_movex = -_movex;
				_gameElements.getGameElement(0)._translate.y = 100;
				_score++;
				std::cout << std::endl << "  YOUR SCORE IS: " << _score << std::endl;
			}
			if (_gameElements.getGameElement(1)._translate.y > _gameElements.getGameElement(i)._translate.y - 2 && _gameElements.getGameElement(1)._translate.y < _gameElements.getGameElement(i)._translate.y - 1
				&& _gameElements.getGameElement(1)._translate.x == _gameElements.getGameElement(i)._translate.x) {
				_movex = -_movex;
				_gameElements.getGameElement(i)._translate.y = 100;
				_score++;
				std::cout << std::endl << "  YOUR SCORE IS: " << _score << std::endl;
			}

		}
	}

	//Execute the automatic actions of the game engine

	if (counter == 50)
	{
		//(_gameElements.getGameElement(2)._angle) = (_gameElements.getGameElement(2)._angle) + 30;
		//(_gameElements.getGameElement(2)._rotation.z) = 1.0f;

		/*cameras[0] = new Camera(_screenWidth / _screenHeight, 3.0f, 300.0f, 0.1f, 30.0f, 30.0f, true, glm::vec3(0.5f, 0.5f, 0.0f), glm::vec3(0.0f, 0.0f, 0.5f));
		
		(_gameElements.getGameElement(2)._angle) = (_gameElements.getGameElement(2)._angle) + 30;
		(_gameElements.getGameElement(2)._rotation.z) = 1.0f;*/

		counter = 0;
	}
	counter++;
}

/**
* Draw the sprites on the screen
*/
void Game::renderGame() {
		//Temporal variable
	GameObject currentRenderedGameElement;

		//Clear the color and depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		//Bind the GLSL program. Only one code GLSL can be used at the same time
	_colorProgram.use();

	//Activate and Bind Texture
	glActiveTexture(GL_TEXTURE0);

	glUniformMatrix4fv(_viewMatrixUniform, 1, GL_FALSE, glm::value_ptr(_cameras[_cameraID].getViewMatrix()));

	glUniformMatrix4fv(_projectionMatrixUniform, 1, GL_FALSE, glm::value_ptr(_cameras[_cameraID].getProjectionMatrix()));

	glUniform1i(_drawModeUniform, _drawMode);
	
	glUniform1i(_lightModeUniform, _lightMode);
	
	
	glUniform3fv(_lightPosition, 1, glm::value_ptr(_gameElements.getGameElement(1)._translate));

	glUniform3fv(_viewerPosition, 1, glm::value_ptr(_cameras[_cameraID]._cameraPos));

	material currentLightColor;
	currentLightColor = _materialManager.getMaterialComponents(0);

	glUniform3fv(_lightColorAmbientUniform, 1, glm::value_ptr(currentLightColor.ambient));
	glUniform3fv(_lightColorDiffuseUniform, 1, glm::value_ptr(currentLightColor.diffuse));
	glUniform3fv(_lightColorSpecularUniform, 1, glm::value_ptr(currentLightColor.specular));


	material currentMaterial;
	

	//For each one of the elements: Each object MUST BE RENDERED based on its position, rotation and scale data
	for (int i = 0; i < _gameElements.getNumGameElements(); i++) {			
		currentRenderedGameElement = _gameElements.getGameElement(i);		
			//TODO: Compute its model transformation matrix
		glm::mat4 modelMatrix;
		modelMatrix = glm::translate(modelMatrix, currentRenderedGameElement._translate);

		if (currentRenderedGameElement._angle != 0)
		{
			modelMatrix = glm::rotate(modelMatrix, glm::radians(currentRenderedGameElement._angle), currentRenderedGameElement._rotation);
		}
		if (currentRenderedGameElement._materialID == 0) {
			glUniform1i(_isALightSource, 1);
			
		}
		else if (currentRenderedGameElement._materialID != 0) {
			glUniform1i(_isALightSource, 0);
		}


		modelMatrix = glm::scale(modelMatrix, currentRenderedGameElement._scale);
			
		////
		glm::mat3 modelNormalMatrix = glm::mat3(glm::transpose(glm::inverse(modelMatrix)));


		//TODO: Pass the matrix as an uniform variable 
		glUniformMatrix4fv(_modelMatrixUniform, 1, GL_FALSE, glm::value_ptr(modelMatrix));

		glUniformMatrix3fv(_modelNormalMatrixUniform, 1, GL_FALSE, glm::value_ptr(modelNormalMatrix));
		

		//Texture
		glBindTexture(GL_TEXTURE_2D, currentRenderedGameElement._textureID);

		glUniform4fv(_newColorUniform, 1, glm::value_ptr(currentRenderedGameElement._color));
		glUniform1i(_textureDataLocation, 0);		//This line is not needed if we use only 1 texture, it is sending the GL_TEXTURE0		
		if (currentRenderedGameElement._textureRepetion) {
			glUniform2f(_textureScaleFactorLocation, currentRenderedGameElement._scale.x, currentRenderedGameElement._scale.y);
		}
		else {
			glUniform2f(_textureScaleFactorLocation, 1.0f, 1.0f);
		}

			//Send data to GPU
		_openGLBuffers.sendDataToGPU(_gameElements.getData(currentRenderedGameElement._objectType), _gameElements.getNumVertices(currentRenderedGameElement._objectType));

		//Unbind the texture
		glBindTexture(GL_TEXTURE_2D, 0);

		currentMaterial = _materialManager.getMaterialComponents(currentRenderedGameElement._materialID);
		glUniform3fv(_materialAmbientUniform, 1, glm::value_ptr(currentMaterial.ambient));
		glUniform3fv(_materialDiffuseUniform, 1, glm::value_ptr(currentMaterial.diffuse));
		glUniform3fv(_materialSpecularUniform, 1, glm::value_ptr(currentMaterial.specular));
		if (i != 1) glUniform1f(_materialShininessUniform, currentMaterial.shininess);


	}

	//Unbind the program
	_colorProgram.unuse();


	


	//Swap the display buffers (displays what was just drawn)
	_window.swapBuffer();
}


