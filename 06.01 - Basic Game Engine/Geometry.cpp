#include "Geometry.h"
#include "ErrorManagement.h"
#include <iostream>
#include <fstream>
#define GLM_FORCE_RADIANS
#include <glm\glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace std;


/*
* Constructor 
*/
Geometry::Geometry(){
	
	glm::vec3 n;
	glm::vec3 AB;
	glm::vec3 AC;

	for (int i = 0; i < NUMBASICOBJECTS; i++)
	{
		_numVertices[i] = 36;
		_verticesData[i] = new Vertex[36];

		//Cara 1 La que vemos desde delante
		_verticesData[i][0].setPosition(-1, 1, 1);
		_verticesData[i][0].setUV(0, 1);
		_verticesData[i][1].setPosition(0, 1, 1);
		_verticesData[i][1].setUV(1, 1);
		_verticesData[i][2].setPosition(-1, 0, 1);
		_verticesData[i][2].setUV(0, 0);
		 
	/*	AB = glm::vec3(_verticesData[i][1].position.x - _verticesData[i][0].position.x, _verticesData[i][1].position.y - _verticesData[i][0].position.y, _verticesData[i][1].position.z - _verticesData[i][0].position.z);
		AC = glm::vec3(_verticesData[i][2].position.x - _verticesData[i][0].position.x, _verticesData[i][2].position.y - _verticesData[i][0].position.y, _verticesData[i][2].position.z - _verticesData[i][0].position.z);
		n = glm::cross(AB, AC);
		n = glm::normalize(n);*/
		glm::vec3 potato(-1, 1, 1); 
		potato = glm::normalize(potato);
		_verticesData[i][0].setNormal(potato.x, potato.y, potato.z);
		potato = glm::vec3(0, 1, 1);
		potato = glm::normalize(potato);
		_verticesData[i][1].setNormal(potato.x, potato.y, potato.z);
		potato = glm::vec3(-1, 0, 1);
		potato = glm::normalize(potato);
		_verticesData[i][2].setNormal(potato.x, potato.y, potato.z);

		_verticesData[i][3].setPosition(0, 1, 1);
		_verticesData[i][3].setUV(1, 1);
		_verticesData[i][4].setPosition(0, 0, 1);
		_verticesData[i][4].setUV(1, 0);
		_verticesData[i][5].setPosition(-1, 0, 1);
		_verticesData[i][5].setUV(0, 0);

	/*	AB = glm::vec3(_verticesData[i][4].position.x - _verticesData[i][3].position.x, _verticesData[i][4].position.y - _verticesData[i][3].position.y, _verticesData[i][4].position.z - _verticesData[i][3].position.z);
		AC = glm::vec3(_verticesData[i][5].position.x - _verticesData[i][3].position.x, _verticesData[i][5].position.y - _verticesData[i][3].position.y, _verticesData[i][5].position.z - _verticesData[i][3].position.z);
		n = glm::cross(AB, AC);
		n = glm::normalize(n);*/
		potato = glm::vec3(0, 1, 1);
		potato = glm::normalize(potato);
		_verticesData[i][3].setNormal(potato.x, potato.y, potato.z);
		potato = glm::vec3(0, 0, 1);
		potato = glm::normalize(potato);
		_verticesData[i][4].setNormal(potato.x, potato.y, potato.z);
		potato = glm::vec3(-1, 0, 1);
		potato = glm::normalize(potato);
		_verticesData[i][5].setNormal(potato.x, potato.y, potato.z);

		//Cara 2 la que est� arriba
		_verticesData[i][6].setPosition(-1, 0, 1);
		_verticesData[i][6].setUV(0, 0);
		_verticesData[i][7].setPosition(0, 0, 1);
		_verticesData[i][7].setUV(1, 0);
		_verticesData[i][8].setPosition(-1, 0, 0);
		_verticesData[i][8].setUV(0, 1);

		/*AB = glm::vec3(_verticesData[i][7].position.x - _verticesData[i][6].position.x, _verticesData[i][7].position.y - _verticesData[i][6].position.y, _verticesData[i][7].position.z - _verticesData[i][6].position.z);
		AC = glm::vec3(_verticesData[i][8].position.x - _verticesData[i][6].position.x, _verticesData[i][8].position.y - _verticesData[i][6].position.y, _verticesData[i][8].position.z - _verticesData[i][6].position.z);
		n = glm::cross(AB, AC);
		n = glm::normalize(n);*/
		potato = glm::vec3(-1, 0, 1);
		potato = glm::normalize(potato);
		_verticesData[i][6].setNormal(potato.x, potato.y, potato.z);
		potato = glm::vec3(0, 0, 1);
		potato = glm::normalize(potato);
		_verticesData[i][7].setNormal(potato.x, potato.y, potato.z);
		potato = glm::vec3(-1, 0, 0);
		potato = glm::normalize(potato);
		_verticesData[i][8].setNormal(potato.x, potato.y, potato.z);


		_verticesData[i][9].setPosition(-1, 0, 0);
		_verticesData[i][9].setUV(1, 0);
		_verticesData[i][10].setPosition(0, 0, 1);
		_verticesData[i][10].setUV(1, 1);
		_verticesData[i][11].setPosition(0, 0, 0);
		_verticesData[i][11].setUV(0, 1);

		/*AB = glm::vec3(_verticesData[i][10].position.x - _verticesData[i][9].position.x, _verticesData[i][10].position.y - _verticesData[i][9].position.y, _verticesData[i][10].position.z - _verticesData[i][9].position.z);
		AC = glm::vec3(_verticesData[i][11].position.x - _verticesData[i][9].position.x, _verticesData[i][11].position.y - _verticesData[i][9].position.y, _verticesData[i][11].position.z - _verticesData[i][9].position.z);
		n = glm::cross(AB, AC);
		n = glm::normalize(n);*/
		potato = glm::vec3(-1, 0, 0);
		potato = glm::normalize(potato);
		_verticesData[i][9].setNormal(potato.x, potato.y, potato.z);
		potato = glm::vec3(0, 0, 1);
		potato = glm::normalize(potato);
		_verticesData[i][10].setNormal(potato.x, potato.y, potato.z);
		potato = glm::vec3(0, 0, 0);
		potato = glm::normalize(potato);
		_verticesData[i][11].setNormal(potato.x, potato.y, potato.z);


		//Cara 3 la que est� abajo
		_verticesData[i][12].setPosition(-1, 1, 1);
		_verticesData[i][12].setUV(0, 0);
		_verticesData[i][13].setPosition(-1, 1, 0);
		_verticesData[i][13].setUV(1, 0);
		_verticesData[i][14].setPosition(0, 1, 1);
		_verticesData[i][14].setUV(0, 1);

		/*AB = glm::vec3(_verticesData[i][13].position.x - _verticesData[i][12].position.x, _verticesData[i][13].position.y - _verticesData[i][12].position.y, _verticesData[i][13].position.z - _verticesData[i][12].position.z);
		AC = glm::vec3(_verticesData[i][14].position.x - _verticesData[i][12].position.x, _verticesData[i][14].position.y - _verticesData[i][12].position.y, _verticesData[i][14].position.z - _verticesData[i][12].position.z);
		n = glm::cross(AB, AC);
		n = glm::normalize(n);*/
		potato = glm::vec3(-1, 1, 1);
		potato = glm::normalize(potato);
		_verticesData[i][12].setNormal(potato.x, potato.y, potato.z);
		potato = glm::vec3(-1, 1, 0);
		potato = glm::normalize(potato);
		_verticesData[i][13].setNormal(potato.x, potato.y, potato.z);
		potato = glm::vec3(0, 1, 1);
		potato = glm::normalize(potato);
		_verticesData[i][14].setNormal(potato.x, potato.y, potato.z);


		_verticesData[i][15].setPosition(-1, 1, 0);
		_verticesData[i][15].setUV(1, 0);
		_verticesData[i][16].setPosition(0, 1, 0);
		_verticesData[i][16].setUV(1, 1);
		_verticesData[i][17].setPosition(0, 1, 1);
		_verticesData[i][17].setUV(0, 1);

		/*AB = glm::vec3(_verticesData[i][16].position.x - _verticesData[i][15].position.x, _verticesData[i][16].position.y - _verticesData[i][15].position.y, _verticesData[i][16].position.z - _verticesData[i][15].position.z);
		AC = glm::vec3(_verticesData[i][17].position.x - _verticesData[i][15].position.x, _verticesData[i][17].position.y - _verticesData[i][15].position.y, _verticesData[i][17].position.z - _verticesData[i][15].position.z);
		n = glm::cross(AB, AC);
		n = glm::normalize(n);*/
		potato = glm::vec3(-1, 1, 0);
		potato = glm::normalize(potato);
		_verticesData[i][15].setNormal(potato.x, potato.y, potato.z);
		potato = glm::vec3(0, 1, 0);
		potato = glm::normalize(potato);
		_verticesData[i][16].setNormal(potato.x, potato.y, potato.z);
		potato = glm::vec3(0, 1, 1);
		potato = glm::normalize(potato);
		_verticesData[i][17].setNormal(potato.x, potato.y, potato.z);


		//Cara 4 La de atr�s! ESTA ES LA K VEMOOOOOO
		_verticesData[i][18].setPosition(-1, 0, 0);
		_verticesData[i][18].setUV(0, 0);
		_verticesData[i][19].setPosition(0, 1, 0);
		_verticesData[i][19].setUV(1, 1);
		_verticesData[i][20].setPosition(-1, 1, 0);
		_verticesData[i][20].setUV(0, 1);

		/*AB = glm::vec3(_verticesData[i][19].position.x - _verticesData[i][18].position.x, _verticesData[i][19].position.y - _verticesData[i][18].position.y, _verticesData[i][19].position.z - _verticesData[i][18].position.z);
		AC = glm::vec3(_verticesData[i][20].position.x - _verticesData[i][18].position.x, _verticesData[i][20].position.y - _verticesData[i][18].position.y, _verticesData[i][20].position.z - _verticesData[i][18].position.z);
		n = glm::cross(AB, AC);
		n = glm::normalize(n);*/
		potato = glm::vec3(-1, 0, 0);
		potato = glm::normalize(potato);
		_verticesData[i][18].setNormal(potato.x, potato.y, potato.z);
		potato = glm::vec3(0, 1, 0);
		potato = glm::normalize(potato);
		_verticesData[i][19].setNormal(potato.x, potato.y, potato.z);
		potato = glm::vec3(-1, 1, 0);
		potato = glm::normalize(potato);
		_verticesData[i][20].setNormal(potato.x, potato.y, potato.z);



		_verticesData[i][21].setPosition(-1, 0, 0);
		_verticesData[i][21].setUV(0, 0);
		_verticesData[i][22].setPosition(0, 0, 0);
		_verticesData[i][22].setUV(1, 0);
		_verticesData[i][23].setPosition(0, 1, 0);
		_verticesData[i][23].setUV(1, 1);

		/*AB = glm::vec3(_verticesData[i][22].position.x - _verticesData[i][21].position.x, _verticesData[i][22].position.y - _verticesData[i][21].position.y, _verticesData[i][22].position.z - _verticesData[i][21].position.z);
		AC = glm::vec3(_verticesData[i][23].position.x - _verticesData[i][21].position.x, _verticesData[i][23].position.y - _verticesData[i][21].position.y, _verticesData[i][23].position.z - _verticesData[i][21].position.z);
		n = glm::cross(AB, AC);
		n = glm::normalize(n);*/
		potato = glm::vec3(-1, 0, 0);
		potato = glm::normalize(potato);
		_verticesData[i][21].setNormal(potato.x, potato.y, potato.z);
		potato = glm::vec3(0, 0, 0);
		potato = glm::normalize(potato);
		_verticesData[i][22].setNormal(potato.x, potato.y, potato.z);
		potato = glm::vec3(0, 1, 0);
		potato = glm::normalize(potato);
		_verticesData[i][23].setNormal(potato.x, potato.y, potato.z);


		//Cara 5 La de la izquierda pues
		_verticesData[i][24].setPosition(-1, 1, 1);
		_verticesData[i][24].setUV(0, 0);
		_verticesData[i][25].setPosition(-1, 1, 0);
		_verticesData[i][25].setUV(1, 0);
		_verticesData[i][26].setPosition(-1, 0, 1);
		_verticesData[i][26].setUV(0, 1);

		/*AB = glm::vec3(_verticesData[i][25].position.x - _verticesData[i][24].position.x, _verticesData[i][25].position.y - _verticesData[i][24].position.y, _verticesData[i][25].position.z - _verticesData[i][24].position.z);
		AC = glm::vec3(_verticesData[i][26].position.x - _verticesData[i][24].position.x, _verticesData[i][26].position.y - _verticesData[i][24].position.y, _verticesData[i][26].position.z - _verticesData[i][24].position.z);
		n = glm::cross(AB, AC);
		n = glm::normalize(n);*/
		potato = glm::vec3(-1, 1, 1);
		potato = glm::normalize(potato);
		_verticesData[i][24].setNormal(potato.x, potato.y, potato.z);
		potato = glm::vec3(-1, 1, 0);
		potato = glm::normalize(potato);
		_verticesData[i][25].setNormal(potato.x, potato.y, potato.z);
		potato = glm::vec3(-1, 0, 1);
		potato = glm::normalize(potato);
		_verticesData[i][26].setNormal(potato.x, potato.y, potato.z);


		_verticesData[i][27].setPosition(-1, 0, 1);
		_verticesData[i][27].setUV(1, 0);
		_verticesData[i][28].setPosition(-1, 1, 0);
		_verticesData[i][28].setUV(1, 1);
		_verticesData[i][29].setPosition(-1, 0, 0);
		_verticesData[i][29].setUV(0, 1);
		
		/*AB = glm::vec3(_verticesData[i][28].position.x - _verticesData[i][27].position.x, _verticesData[i][28].position.y - _verticesData[i][27].position.y, _verticesData[i][28].position.z - _verticesData[i][27].position.z);
		AC = glm::vec3(_verticesData[i][29].position.x - _verticesData[i][27].position.x, _verticesData[i][29].position.y - _verticesData[i][27].position.y, _verticesData[i][29].position.z - _verticesData[i][27].position.z);
		n = glm::cross(AB, AC);
		n = glm::normalize(n);*/
		potato = glm::vec3(-1, 0, 1);
		potato = glm::normalize(potato);
		_verticesData[i][27].setNormal(potato.x, potato.y, potato.z);
		potato = glm::vec3(-1, 1, 0);
		potato = glm::normalize(potato);
		_verticesData[i][28].setNormal(potato.x, potato.y, potato.z);
		potato = glm::vec3(-1, 0, 0);
		potato = glm::normalize(potato);
		_verticesData[i][29].setNormal(potato.x, potato.y, potato.z);


		//Cara 6 La de la derecha
		_verticesData[i][30].setPosition(0, 0, 1);
		_verticesData[i][30].setUV(0, 0);
		_verticesData[i][31].setPosition(0, 1, 0);
		_verticesData[i][31].setUV(1, 0);
		_verticesData[i][32].setPosition(0, 1, 1);
		_verticesData[i][32].setUV(0, 1);

		/*AB = glm::vec3(_verticesData[i][31].position.x - _verticesData[i][30].position.x, _verticesData[i][31].position.y - _verticesData[i][30].position.y, _verticesData[i][31].position.z - _verticesData[i][30].position.z);
		AC = glm::vec3(_verticesData[i][32].position.x - _verticesData[i][30].position.x, _verticesData[i][32].position.y - _verticesData[i][30].position.y, _verticesData[i][32].position.z - _verticesData[i][30].position.z);
		n = glm::cross(AB, AC);
		n = glm::normalize(n);*/
		potato = glm::vec3(0, 0, 1);
		potato = glm::normalize(potato);
		_verticesData[i][30].setNormal(potato.x, potato.y, potato.z);
		potato = glm::vec3(0, 1, 0);
		potato = glm::normalize(potato);
		_verticesData[i][31].setNormal(potato.x, potato.y, potato.z);
		potato = glm::vec3(0, 1, 1);
		potato = glm::normalize(potato);
		_verticesData[i][32].setNormal(potato.x, potato.y, potato.z);


		_verticesData[i][33].setPosition(0, 0, 0);
		_verticesData[i][33].setUV(1, 0);
		_verticesData[i][34].setPosition(0, 1, 0);
		_verticesData[i][34].setUV(1, 1);
		_verticesData[i][35].setPosition(0, 0, 1);
		_verticesData[i][35].setUV(0, 1);

		/*AB = glm::vec3(_verticesData[i][34].position.x - _verticesData[i][33].position.x, _verticesData[i][34].position.y - _verticesData[i][33].position.y, _verticesData[i][34].position.z - _verticesData[i][33].position.z);
		AC = glm::vec3(_verticesData[i][35].position.x - _verticesData[i][33].position.x, _verticesData[i][35].position.y - _verticesData[i][33].position.y, _verticesData[i][35].position.z - _verticesData[i][33].position.z);
		n = glm::cross(AB, AC);
		n = glm::normalize(n);*/
		potato = glm::vec3(0, 0, 0);
		potato = glm::normalize(potato);
		_verticesData[i][33].setNormal(potato.x, potato.y, potato.z);
		potato = glm::vec3(0, 1, 0);
		potato = glm::normalize(potato);
		_verticesData[i][34].setNormal(potato.x, potato.y, potato.z);
		potato = glm::vec3(0, 0, 1);
		potato = glm::normalize(potato);
		_verticesData[i][35].setNormal(potato.x, potato.y, potato.z);


		//Color para la megaplataforma
		for (int j = 0; j < _numVertices[i]; j++) {
			if (i == PLATFORM) {
				_verticesData[i][j].setColor(255, 0, 0, 255);
			}
			else if (i == BALL) {
				_verticesData[i][j].setColor(255, 255, 255, 255);
			}
			else if (i == COLLIDERS) {
				_verticesData[i][j].setColor(255, 255, 0, 255);
			}
			else if (i == FONDO) {
				_verticesData[i][j].setColor(50, 255, 0, 255);
			}
		}
	}
}


Geometry::~Geometry(){
	delete _verticesData[PLATFORM];
	delete _verticesData[BALL];
	delete _verticesData[COLLIDERS];
	delete _verticesData[FONDO];
	
}

/*
* Load the game elements from a text file
*/
void Geometry::loadGameElements(char fileName[100]){	
	/* Text format
	<number of game elements>
	<type of game element> <vec3 position> <angle> <vec3 rotation> <vec3 scale>	
	*/
	int numGameElements;
	GameObject tempObject;
	glm::vec3 vector3fElements;
	ifstream file;
	file.open(fileName);

	if (file.is_open()){
		//TODO: Read the content and add it into the data structure
		file >> numGameElements;
		for (int i = 0; i < numGameElements; i++)
		{
			file >> tempObject._objectType;

			file >> tempObject._translate.x;
			file >> tempObject._translate.y;
			file >> tempObject._translate.z;

			file >> tempObject._angle;

			file >> tempObject._rotation.x;
			file >> tempObject._rotation.y;
			file >> tempObject._rotation.z;

			file >> tempObject._scale.x;
			file >> tempObject._scale.y;
			file >> tempObject._scale.z;

			file >> tempObject._textureFile;
			
			file >> tempObject._materialID;

			_listOfObjects.push_back(tempObject);

		}



		file.close();
	}
	else{
		string message = "The file "+ string(fileName)+" doesn't exists";
		ErrorManagement::errorRunTime(message);
	}

}

/*
* Get the vertices data for an specific object
* @param objectID is the identifier of the requested object
* @return Vertex * is an array with all the vertices data
*/
Vertex * Geometry::getData(int objectID){
	return _verticesData[objectID];
}

/*
* Get the number of vertices for an specific object
* @param objectID is the identifier of the requested object
* @return int is the number of vertices
*/

int Geometry::getNumVertices(int objectID){
	return _numVertices[objectID];
}

/*
* Get the number of elements to render
*/
int Geometry::getNumGameElements() {
	return _listOfObjects.size();
}

/*
* Get the number of vertices of an specific game element
* @param objectID is the identifier of the requested object
*/
GameObject & Geometry::getGameElement(int objectID) {
	return (_listOfObjects[objectID]);
}
void Geometry::deleteGameElement(int pos) {
	//_listOfObjects[pos] = 
}

