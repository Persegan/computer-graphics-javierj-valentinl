#pragma once
#include <GL/glew.h>			//The OpenGL Extension Wrangler
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>			//OpenGL Mathematics 
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include "Window.h"
#include "GLSLProgram.h"
#include "FPSLimiter.h"
#include "OpenGLBuffers.h"
#include "Vertex.h"
#include "Geometry.h"
#include "InputManager.h"

class Camera
{
protected:
	float _aspectRatio;
	glm::mat4 _projectionMatrix;
	glm::mat4 _viewMatrix;
	float _FOV;
	float _far;
	float _near;
	float _projectionWidth;
	float _projectionHeight;
	
	glm::vec3 _cameraFront;
	glm::vec3 _cameraUp;
	bool _perspectiveTrue;




public:
	glm::vec3 _cameraPos;

	Camera();
	~Camera();

	void setOrthograficProjMat();
	void setPerspectiveProjMat();

	Camera(float aspectRatio, float FOV, float far, float near, float projectionWidth, float projectionHeight, bool perspectiveTrue, glm::vec3 cameraPos, glm::vec3 cameraFront);
	

	glm::mat4 &getProjectionMatrix();
	glm::mat4 &getViewMatrix();

	void setViewMatrix(glm::vec3 cameraPos, glm::vec3 cameraFront);

	void changeProjType(bool perspectiveTrue);




};