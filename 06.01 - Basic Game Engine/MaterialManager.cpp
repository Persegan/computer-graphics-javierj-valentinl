#pragma once
#include <glm/glm.hpp>			//OpenGL Mathematics 
#include <string>
#include "MaterialManager.h"


void MaterialManager::createMaterialDefinitions() {
	/*	_materialData[PEARL].ambient = glm::vec3(0.25, 0.20725, 0.20725);
	_materialData[PEARL].diffuse = glm::vec3(1, 0.829, 0.829);
	_materialData[PEARL].specular = glm::vec3(0.296648, 0.296648, 0.296648);
	_materialData[PEARL].shininess = 0.088f;
	_materialType[PEARL] = "pearl";*/
	//PEARL
	_materialData[PEARL].ambient = glm::vec3(0.2, 0.2, 0.2);
	_materialData[PEARL].diffuse = glm::vec3(1, 1, 1);
	_materialData[PEARL].specular = glm::vec3(1, 1, 1);
	_materialData[PEARL].shininess = 0.1;
	_materialType[PEARL] = "pearl";

	// SILVER
	/*		_materialData[SILVER].ambient = glm::vec3(0.19225, 0.19225, 0.19225);
	_materialData[SILVER].diffuse = glm::vec3(0.50754, 0.50754, 0.5074);
	_materialData[SILVER].specular = glm::vec3(0.508273, 0.508273, 0.508273);
	_materialData[SILVER].shininess = 0.4f;
	_materialType[SILVER] = "silver";*/
	_materialData[SILVER].ambient = glm::vec3(0.1, 0.2, 0.1);
	_materialData[SILVER].diffuse = glm::vec3(0, 0, 1);
	_materialData[SILVER].specular = glm::vec3(0, 0, 1);
	_materialData[SILVER].shininess = 0.5f;
	_materialType[SILVER] = "silver";


}
MaterialManager::MaterialManager() {
	createMaterialDefinitions();
}
MaterialManager::~MaterialManager() {

}
material MaterialManager::getMaterialComponents(int materialID) {
	return _materialData[materialID];
}
int MaterialManager::getMaterialID(std::string materialName) {
	for (int i = 0; i < NUMMATERIALS; i++) {
		if(_materialType[i] == materialName) 
			return i;
	}
}