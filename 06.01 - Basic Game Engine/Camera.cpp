#include "Camera.h"



Camera::Camera()
{
}

Camera::~Camera()
{
}

void Camera::setOrthograficProjMat() {
	this->_projectionMatrix = (glm::ortho(-(this->_projectionWidth / 2), this->_projectionWidth / 2, -(this->_projectionHeight / 2), (this->_projectionHeight / 2), this->_near, this->_far));
}

void Camera::setPerspectiveProjMat() {
	this->_projectionMatrix = glm::perspective(this->_FOV, this->_aspectRatio, this->_near, this->_far);
}

Camera::Camera(float aspectRatio, float FOV, float far, float near, float projectionWidth, float projectionHeight, bool perspectiveTrue, glm::vec3 cameraPos, glm::vec3 cameraFront ) {
	this->_aspectRatio = aspectRatio;
	this->_FOV = FOV;
	this->_far = far;
	this->_near = near;
	this->_projectionWidth = projectionWidth;
	this->_projectionHeight = projectionHeight;
	this->_perspectiveTrue = perspectiveTrue;
	this->_cameraPos = cameraPos;
	this->_cameraFront = cameraFront;

	setViewMatrix(cameraPos, cameraFront);

	if (_perspectiveTrue) {
		setPerspectiveProjMat();
	}
	else if (!_perspectiveTrue) {
		setOrthograficProjMat();
	}
}


glm::mat4 &Camera::getProjectionMatrix() {
	return this->_projectionMatrix;
}

glm::mat4 &Camera::getViewMatrix() {
	return this->_viewMatrix;
}

void Camera::changeProjType(bool perspectiveTrue) {
	this->_perspectiveTrue = perspectiveTrue;

	if (_perspectiveTrue) {
		setPerspectiveProjMat();
	}
	else if (!_perspectiveTrue) {
		setOrthograficProjMat();
	}
}

void Camera::setViewMatrix(glm::vec3 cameraPos, glm::vec3 cameraFront) {
	this->_cameraPos = cameraPos;
	this->_cameraFront = cameraFront;


	glm::vec3 cameraDirection = glm::normalize(_cameraPos - _cameraFront);
	glm::vec3 up = glm::vec3(0.0f, 0.0f, 1.0f);
	glm::vec3 cameraRight = glm::normalize(glm::cross(up, cameraDirection));
	_cameraUp = glm::cross(cameraDirection, cameraRight);
	_viewMatrix = glm::lookAt(_cameraPos, _cameraFront, _cameraUp);
}